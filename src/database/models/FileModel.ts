import { 
    prop,
    getModelForClass,
    modelOptions
} from "@typegoose/typegoose";

@modelOptions({
    options: {
        allowMixed: 0
    }
})
export class File {
    @prop({ required: true })
    public name!: string;

    @prop({ required: true })
    public Key!: string;

    @prop({ required: true })
    public bucket!: string;

    @prop()
    public metaData?: any;

};

export default getModelForClass(File);