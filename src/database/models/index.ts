import FileModel from "./FileModel";
import DomainModel from "./DomainModel";

export { 
    FileModel,
    DomainModel
};