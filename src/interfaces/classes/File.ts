// s3 and mongodb file api
import { S3 } from "aws-sdk";
import { FileModel } from "../../database/models";
import FileError from "./FileError";
import FileData from "../FileData";


export default class File { 
    s3: S3

    constructor(s3: S3, bucket: string) {
        this.s3 = s3;

        this.read.bind(this);
    };

    async read(name: string): Promise<FileData> {
        const file = await FileModel.findOne({ name });
        if (!file) throw new FileError("File doesn't exist", 404);

        try {
            const data = await this.s3.getObject({
                Bucket: file.bucket,
                Key: file.Key
            }).promise();

            return {
                S3Response: data,
                metaData: file.metaData
            };

        } catch (err) {
            throw err;
        };
    }; 
};