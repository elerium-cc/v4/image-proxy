import config from './utils/dotenv';
config();
import { connect } from "mongoose";
import express from "express";
import { FileClient } from "./utils/FileConfig";
import { DomainModel } from "./database/models";

const app = express();

app.use(async (req, res, next) => {
    const { host } = req.headers;
    const segments = host!.split(".");
    const [ , name, tld ] = segments;
    
    if (await DomainModel.findOne({ name: host })) return next();
    if (await DomainModel.findOne({ name: `${name}.${tld}`})) return next();

    res.redirect(process.env.REDIRECT_DOMAIN!);
});

app.all("/", (req, res) => res.redirect(process.env.REDIRECT_DOMAIN!));

app.get("/:file", async (req, res) => {
    const { params } = req;

    try {
        const { S3Response } = await FileClient.read(params.file);

        res.setHeader("Content-Type", S3Response.ContentType!);
        res.setHeader("Cache-Control", "no-store");
        res.send(S3Response.Body);

    } catch (err) {
        res.status(
            err.code && 
            typeof err.code == "number" ? 
            err.code : 400
        ).json({
            success: false,
            message: err.message
        });
    };
});

connect(process.env.MONGODB_URL!, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
}).then(() => app.listen(process.env.PORT));